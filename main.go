package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strings"
	"unicode/utf8"
)

/* constants */
const (
	lenKeys       = 100           /* maximal number of keys */
	iterNum       = 1000000       /* number of iterations */
	numChangesMax = 1             /* value of maximal changes */
	filenameRes   = "results.txt" /* filename for output */
)

/* structure to save gen (substitution) */
type Gen struct {
	sequence map[int32]int32
}

/* variables */
var (
	gen      Gen /* current substitution */
	genNew   Gen /* new substitution */
	arrayUkr = [33]int32{'a', 'б', 'в', 'г', 'ґ', 'д', 'e', 'є', 'ж', 'з', 'и', 'і', 'ї', 'й', 'к', 'л', 'м',
		'н', 'о', 'п', 'р', 'c', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ь', 'ю', 'я'} /* Ukrainian letters */
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to write information about hash table */
func writeHash(hash map[int32]int32) {

	var (
		k, v int32
	)

	/* go through all elements of hash table */
	for k, v = range hash {
		fmt.Printf("key[%c] - value[%c]\n", k, v)
	}

}

/* function to read file */
func readFile(filename string) string {

	/* declaration of variables */
	var (
		err       error
		fileInput *os.File
		line      []byte
		row       string
	)

	/* open file for reading */
	fileInput, err = os.Open(filename)
	handleError(err)

	/* read all file */
	line, err = ioutil.ReadAll(fileInput)
	handleError(err)

	/* convert to string and return answer */
	row = string(line)
	row = strings.ToLower(row)
	return row

}

/* function to read keys from file */
func readKeys(filename string) []string {

	/* declaration of variables */
	var (
		err     error
		fileKey *os.File
		line    []byte
		row     string
		answer  []string
		reader  *bufio.Reader
	)

	/* crate "answer" array */
	answer = make([]string, 0, lenKeys)

	/* open file for reading */
	fileKey, err = os.Open(filename)
	handleError(err)

	/* create reader and read file */
	reader = bufio.NewReader(fileKey)

	for {

		/* read one line */
		line, _, err = reader.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		/* convert to string */
		row = string(line)
		answer = append(answer, row)

	}

	/* return "answer" */
	return answer

}

/* function to read row */
func readRow(msg string, row *string) {

	var (
		err error
	)

	/* write message and read row */
	fmt.Println(msg)
	_, err = fmt.Scanln(row)
	handleError(err)

}

/* function to create initial state */
func createInitialState() {

	var (
		i int
	)

	/* crate initial state */
	gen.sequence = make(map[int32]int32)
	genNew.sequence = make(map[int32]int32)
	i = 0
	for i < len(arrayUkr) {
		gen.sequence[arrayUkr[i]] = arrayUkr[i]
		i++
	}

}

/* function to find word in text */
func findWordInText(text string, key string) int {

	var (
		i, i1 int
		ind   bool
		sum   int
	)

	/* go through all text */
	i = 0
	sum = 0
	for i <= len(text)-len(key) {
		ind = true
		i1 = 0
		/* find word */
		for i1 < len(key) {
			if text[i+i1] != key[i1] {
				ind = false
				break
			}
			i1++
		}
		if ind {
			sum += len(key)
		}
		i++
	}

	/* return answer */
	return sum

}

/* function to calculate words in text */
func calcWordsInText(text string, keys []string) int {

	var (
		i   int
		sum int
	)

	/* go through all words */
	i = 0
	sum = 0
	for i < len(keys) {
		sum += findWordInText(text, keys[i])
		i++
	}

	/* return answer */
	return sum

}

/* do transformation with text (substitution) */
func doTransform(text string, gen Gen) string {

	var (
		modText   string
		i         int
		w         int
		symbol    int32
		newSymbol int32
		width     int
	)

	/* go through all text */
	w = 0
	modText = ""
	for i = 0; i < len(text); i += w {
		/* do transformation */
		symbol, width = utf8.DecodeRuneInString(text[i:])
		if _, ok := gen.sequence[symbol]; ok {
			newSymbol = gen.sequence[symbol]
		} else {
			newSymbol = symbol
		}
		modText = modText + string(newSymbol)
		w = width
	}

	/* return modified text */
	return modText

}

/* function to change gen (substitution) */
func modifyGen(genOld Gen, genNew Gen, iter int) {

	var (
		i          int
		numChanges int
		randNum1   int
		randNum2   int
		symbol1    int32
		symbol2    int32
		symbolRes  int32
		k, v       int32
	)

	/* calculate number of changes */
	numChanges = numChangesMax / (iter/1000 + 1)
	if numChanges == 0 {
		numChanges = 1
	}

	/* copy old gen to new gen */
	for k = range genNew.sequence {
		delete(genNew.sequence, k)
	}
	for k, v = range genOld.sequence {
		genNew.sequence[k] = v
	}

	/* make all changes */
	i = 0
	for i < numChanges {
		randNum1 = rand.Intn(len(genOld.sequence))
		randNum2 = rand.Intn(len(genOld.sequence))
		symbol1 = arrayUkr[randNum1]
		symbol2 = arrayUkr[randNum2]
		symbolRes = genNew.sequence[symbol1]
		genNew.sequence[symbol1] = genNew.sequence[symbol2]
		genNew.sequence[symbol2] = symbolRes
		i++
	}

}

/* function to copy gens */
func copyGen(genOld Gen, genNew Gen) {

	var (
		k, v int32
	)

	/* copy new gen to old gen */
	for k = range genOld.sequence {
		delete(genOld.sequence, k)
	}
	for k, v = range genNew.sequence {
		genOld.sequence[k] = v
	}

}

/* do search */
func doSearch(text string, keys []string) {

	var (
		i          int
		sum        int
		sumNew     int
		modText    string
		modTextNew string
	)

	/* modify and save new gen */
	modifyGen(gen, genNew, 2)
	copyGen(gen, genNew)

	i = 0
	for i < iterNum {

		/* modify old gen */
		modifyGen(gen, genNew, i+1)

		/* create modified texts */
		modText = doTransform(text, gen)
		modTextNew = doTransform(text, genNew)

		/* calculate "quality" of text */
		sum = calcWordsInText(modText, keys)
		sumNew = calcWordsInText(modTextNew, keys)

		/* change gen if new variant is better */
		if sumNew > sum {
			copyGen(gen, genNew)
		}

		i++

	}

}

/* function to write results */
func writeResults(text string) {

	var (
		modText    string
		err        error
		fileOutput *os.File
	)

	/* do transformation */
	modText = doTransform(text, gen)

	/* create file to write results */
	fileOutput, err = os.Create(filenameRes)
	handleError(err)

	/* write final text */
	_, err = fileOutput.WriteString(modText)
	handleError(err)

	/* write hash */
	writeHash(gen.sequence)

}

/* entry point */
func main() {

	var (
		filenameInput string
		filenameKey   string
		keys          []string
		text          string
	)

	/* read filenames */
	readRow("Input file:", &filenameInput)
	readRow("Key file:", &filenameKey)

	/* read texts and keys */
	text = readFile(filenameInput)
	keys = readKeys(filenameKey)

	/* create initial state */
	createInitialState()

	/* do search */
	doSearch(text, keys)

	/* write results */
	writeResults(text)

}
